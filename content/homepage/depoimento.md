---
title: "Depoimentos"
weight: 3
header_menu: true
---

Sinta-se a vontade de compartilhar conosco a sua experiência com autoritarismo e o assédio moral. Seu relato é muito importante no combate e na conscientização. Caso seja a sua vontade, seu depoimento ficará completamente anônimo para protegê-lo.

{{< rawhtml >}}
<form action="https://www.formbackend.com/f/79b1085600c1ef9c" method="POST">

  <p>
    <label for="name">Nome</label>
    <input style="padding: 0.3em;width: 65%;" type="text" name="name" id="name" placeholder="Fulano da silva" required>
  </p>
  <p>
  <label for="email">Email</label>
  <input style="padding: 0.3em;width: 65%;" type="email" name="email" id="email" placeholder="meuemail@gmail.com" required>
  </p>
  
  <p>
    <label for="mensagem">Escreva nesta caixa a sua experiência.</label><br>
    <textarea style="padding: 0.3em; width:90%; margin: 0 auto"  rows="10" name="mensagem" id="mensagem" class="inputPadrao" required placeholder="Escreva aqui o seu relato."></textarea>
  </p>
  <button type="submit">Enviar</button>

</form>
{{< /rawhtml >}}