---
title: "Eventos"
weight: 2
header_menu: true
---

##### Bem-estar no ambinte acadêmico: contra o autoritarismo e o assédio moral

Este evento será um conversatório durante uma tarde, vamos desenvolver reflexões sobre a cultura autoritária e assédio moral e sua intersecção com a discriminação sexual, por raça e por gênero. O evento é uma proposta articulada com diversas entidades dos trabalhadores da UFBA, como APUB e ASSUFBA, e entidades e órgãos governamentais, como Defensoria Pública do Estado da Bahia e Ministério Público do Trabalho, e entidades da Administração Central da UFBA, como a PRODEP. Neste evento, será o lançamento oficial do site do coletivo desnaturaliza, onde a comunidade poderá relatar as violências que já sofreram, e do questionário para fazer o levantamento estatístico dos dados sobre assédio. O foco deste levantamento será inicialmente a Universidade Federal da Bahia.

**Data de realização:** A marcar.
