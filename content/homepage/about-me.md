---
title: "O Coletivo Desnaturaliza"
weight: 1
header_menu: true
---

O assédio e a cultura autoritária são geralmente associados a ambiente laborais, e as entidades de classe tem um longo histórico de luta contra o assédio moral. No legislativo federal, temos
[o projeto de lei  1521 de 2019](https://www25.senado.leg.br/web/atividade/materias/-/materia/135758).
De acordo com este projeto de lei, que foi aprovado na Câmara Federal no dia 12/03/2019, o assédio moral é caracterizado por ofender reiteradamente a dignidade de alguém causando-lhe dano ou sofrimento físico ou mental, no exercício de emprego, cargo ou função. Como nota-se pela data do projeto de lei, o assédio moral e a cultura autoritária são assuntos poucos discutidos e abordados pelo poder legislativo e pelos empregadores. Em uma linha semelhante, o [Tribunal Superior do Trabalho](http://www.tst.jus.br/documents/10157/55951/Cartilha+assédio+moral/573490e3-a2dd-a598-d2a7-6d492e4b2457) definiu como a "a exposição de pessoas a situações humilhantes e constrangedoras no ambiente de trabalho, de forma repetitiva e prolongada, no exercício de suas atividades. É uma conduta que traz danos à dignidade e à integridade do indivíduo, colocando a saúde em risco e prejudicando o ambiente de trabalho." As entidades de classe fizeram um excelente trabalho na conscientização da cultura autoritária e o assédio moral no ambiente de trabalho, mas essa prática também acontece em diversos outros ambientes como o universitário e acadêmico.

O objetivo deste coletivo é expandir esta discussão sob assédio e cultura autoritária no ambiente acadêmico. Acreditamos que o primeiro passo para que esta prática tóxica que causa adoecimento físico e mental em nossa comunidade seja superada é através de dois passos:

{{< rawhtml >}}

<ol>
  <li><strong>Conscientização:</strong> realização de ações e atividades para explicar e sensibilizar todos os membros de nossa comunidade sobre o assédio moral e a cultura autoritária, principalmente aquelas que perpassam pelas violências raciais, sexuais e de gênero;</li>
  <li><strong>Combate:</strong> criação de comissões e políticas de combate ao assédio moral pelas unidades universitárias e pela Administração Central da UFBA.</li>
</ol>

{{< /rawhtml >}}